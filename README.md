# README

This is a simple static webpage that shows some work I did on an Android app for Smartlane.

## App Information

App Name: smartlane

Created: Spring 2017

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/smartlane)

Production: [Link](https://smartlane.mctighecw.site)

## Tech Stack

* HTML
* CSS
* Bootstrap

Last updated: 2024-08-04
